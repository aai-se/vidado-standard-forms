# A2019 Vidado Package
_Standard Forms with Vidado A2019 Package._

# WARNING: THIS IS BETA VERSION

## Features
 * Generates single JAR file package that is importable in to an A2019 Control Room.
 * Adds the "Vidado Standard Forms" package within A2019 task bot edit/creation with the following options -
    * Create Batch
    * Upload Documents to Batch
    * Submit Batch
    * Get Batch Info
    * Get List of Cases
    * Get Case Count
    * Get Details of a Case (as JSON or CSV or as a List of Dictionaries)
    * Update a Case to "exported"
    
    
### Building
##### ALL dependencies needed for building the JAR are included in the package.
1. Clone repo into IntelliJ.
2. Run `gradle assemble` to generate the package.json.
3. Run `gradlew shadowJar` to create the importable fat JAR within the build>libs directory.



## FAQ
### How do I use this?
1. Follow Building instructions above.

2. Import the generated JAR into your A2019 CR-
    1. Log in as a CR Admin.
    2. Go to Bots>Packages.
    3. Click on "Add a Package" in the upper right corner.
    4. Browse to the generated JAR.
    5. Click "Upload Package." 
    6. Click "Accept, enable and set as default."

