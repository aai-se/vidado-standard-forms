package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetCaseCountCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetCaseCountCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetCaseCount command = new GetCaseCount();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("WorkflowID") && parameters.get("WorkflowID") != null && parameters.get("WorkflowID").get() != null) {
      convertedParameters.put("WorkflowID", parameters.get("WorkflowID").get());
      if(!(convertedParameters.get("WorkflowID") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","WorkflowID", "Number", parameters.get("WorkflowID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("WorkflowID") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","WorkflowID"));
    }

    if(parameters.containsKey("FilterOnBatchID") && parameters.get("FilterOnBatchID") != null && parameters.get("FilterOnBatchID").get() != null) {
      convertedParameters.put("FilterOnBatchID", parameters.get("FilterOnBatchID").get());
      if(!(convertedParameters.get("FilterOnBatchID") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","FilterOnBatchID", "Boolean", parameters.get("FilterOnBatchID").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("FilterOnBatchID") != null && (Boolean)convertedParameters.get("FilterOnBatchID")) {
      if(parameters.containsKey("BatchID") && parameters.get("BatchID") != null && parameters.get("BatchID").get() != null) {
        convertedParameters.put("BatchID", parameters.get("BatchID").get());
        if(!(convertedParameters.get("BatchID") instanceof String)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BatchID", "String", parameters.get("BatchID").get().getClass().getSimpleName()));
        }
      }
      if(convertedParameters.get("BatchID") == null) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","BatchID"));
      }

    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(Number)convertedParameters.get("WorkflowID"),(Boolean)convertedParameters.get("FilterOnBatchID"),(String)convertedParameters.get("BatchID")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
