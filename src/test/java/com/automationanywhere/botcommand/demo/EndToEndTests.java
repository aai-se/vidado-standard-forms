package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.util.HashMap;
import java.util.Map;

public class EndToEndTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";
        String BatchName = "Sept3-eoe1";

        String FilePath1 = "C:/iqbot/Vidado/2019-02-03.pdf";
        String FilePath2 = "C:/iqbot/Vidado/2019-02-04.pdf";
        String FilePath3 = "C:/iqbot/Vidado/2019-02-05.pdf";
        String FilePath4 = "C:/iqbot/Vidado/2019-02-06.pdf";

        CreateBatch createbatch = new CreateBatch();
        UploadFileToBatch uploadfiles = new UploadFileToBatch();
        SubmitBatch submitbatch = new SubmitBatch();

        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);

        createbatch.setSessions(mso);
        uploadfiles.setSessions(mso);
        submitbatch.setSessions(mso);

        System.out.println("DEBUG - Creating Batch..");
        DictionaryValue d = createbatch.action(SessionName,BatchName);
        Map<String, Value> ResultMap = d.get();

        Value v = ResultMap.get("id");
        String BatchID = v.get().toString();

        System.out.println("DEBUG - Batch ID:"+BatchID);

        uploadfiles.action(SessionName,BatchID,FilePath1);
        //uploadfiles.action(SessionName,BatchID,FilePath2);

        System.out.println("DEBUG - File Uploaded");

        DictionaryValue d1 =  submitbatch.action(SessionName,BatchID);
        Map<String, Value> ResultMap1 = d1.get();

        for (String i : ResultMap1.keySet()) {
            Value SomeValue = ResultMap1.get(i);
            String ID = SomeValue.get().toString();
            System.out.println("Key: "+i+" | Value: "+ID);
        }


    }
}
