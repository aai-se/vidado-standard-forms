package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetCaseDataTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";
        Number CaseID = 6198003;

        GetCaseData command = new GetCaseData();
        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);
        //System.exit(0);
        StringValue d = command.action(SessionName,CaseID);


        System.out.println("Results: "+d.get());


    }
}
