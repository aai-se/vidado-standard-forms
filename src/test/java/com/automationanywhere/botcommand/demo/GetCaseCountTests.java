package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;

import java.util.HashMap;
import java.util.Map;

public class GetCaseCountTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";

        GetCaseCount command = new GetCaseCount();
        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);
        //System.exit(0);
        Number num = 199D;

        NumberValue d = command.action(SessionName,num,false,"");

            System.out.println("Case Count: "+d.get().toString());


    }
}
