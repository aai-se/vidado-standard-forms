package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetCaseListTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";

        GetCaseList command = new GetCaseList();
        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);
        //System.exit(0);
        Number wfNum = 199;
        Number myLimit = 10;
        Number myOffset = 0;
        ListValue d = command.action(SessionName,wfNum,myLimit,myOffset,false, "");
        List<Value> theList = d.get();
        for(int i=0;i<theList.size();i++){

            Map<String, Value> ResultMap = (Map<String, Value>) theList.get(i);
            // (Map<String, Value>) v.get();

            for (String j : ResultMap.keySet()) {
                Value SomeValue = ResultMap.get(j);
                String ID = SomeValue.get().toString();
                System.out.println("Key: "+j+" | Value: "+ID);
            }
        }


    }
}
