package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.core.security.SecureString;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CreateBatchTestIgnore {

    CreateBatch command = new CreateBatch();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"1"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String SomeStr){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";

        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        UUID uuid = UUID.randomUUID();
        System.out.println(uuid);
        System.exit(0);
        DictionaryValue d = command.action(SessionName,"batchname");
        Map<String,Value> ResultMap = d.get();

        Value BatchID = ResultMap.get("id");
        System.out.println("Batch ID:"+BatchID.toString());

    }
}
