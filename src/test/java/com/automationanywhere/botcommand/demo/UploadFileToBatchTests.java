package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UploadFileToBatchTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";
        String BatchID = "9853846";
        String FilePath = "C:/iqbot/Vidado/Form 4562_1-2019-2.pdf";

        UploadFileToBatch command = new UploadFileToBatch();
        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        DictionaryValue d = command.action(SessionName,BatchID,FilePath);
        Map<String, Value> ResultMap = d.get();

        for (String i : ResultMap.keySet()) {
            Value SomeValue = ResultMap.get(i);
            String ID = SomeValue.get().toString();
            System.out.println("Key: "+i+" | Value: "+ID);
        }

    }
}
