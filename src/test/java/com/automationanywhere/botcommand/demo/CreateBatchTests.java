package com.automationanywhere.botcommand.demo;

import Utils.FuncUtils;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreateBatchTests {

    public static void main(String[] args){

        String Token = "62c75b302b3548c694ea723d4cf5a642";
        String APIVersion = "v1";
        String SessionName = "Default";

        CreateBatch command = new CreateBatch();
        VidadoSession myBackendServ = new VidadoSession(FuncUtils.StringToSecureString(Token),APIVersion);
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put(SessionName,myBackendServ);
        command.setSessions(mso);

        UUID uuid = UUID.randomUUID();
        String ShortUUID = uuid.toString().substring(0,8);
        System.out.println(ShortUUID);
        //System.exit(0);
        DictionaryValue d = command.action(SessionName,"batch-"+ShortUUID);
        Map<String, Value> ResultMap = d.get();

        for (String i : ResultMap.keySet()) {
            Value SomeValue = ResultMap.get(i);
            String ID = SomeValue.get().toString();
            System.out.println("Key: "+i+" | Value: "+ID);
        }

    }
}
