package Utils;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class RestRequests {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public RestRequests(){ }

    public org.json.simple.JSONArray UpdateCasesToExported(VidadoSession session, String CaseIDs) throws IOException, ParseException {

        String FullURL = session.getURL()+"/cases/update_statuses_to_exported";

        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(FullURL);
            String payload = "{" + "\"case_ids\": ["+CaseIDs+"] " + "}";
            StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);
            request.setEntity(entity);

            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            org.json.simple.JSONArray jobj = (org.json.simple.JSONArray) parse.parse(JsonResponse);
            return jobj;

        } catch (Exception e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
        } finally {
        }

    }

    public JSONObject GetCaseInfo(VidadoSession session,Number CaseID) throws IOException, ParseException {

        String FullURL = session.getURL()+"/cases/"+CaseID.intValue()+"/data";

        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(FullURL);

            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            return jobj;

        } catch (Exception e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
        } finally {
        }

    }

    public JSONObject GetCaseList(VidadoSession session,Number WorkflowID, Number Limit, Number Offset, String Status) throws IOException, ParseException {

        String FullURL = session.getURL()+"/cases?workflow_id="+WorkflowID.intValue()+"&limit="+Limit.intValue()+"&offset="+Offset.intValue()+"&status="+Status;

        //System.out.println("DEBUG Case Num:"+FullURL);
        HttpClient httpClient = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet(FullURL);

            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            //System.out.println("DEBUG:"+JsonResponse);
            return jobj;
    }

    public JSONObject GetCaseListFromBatchID(VidadoSession session,Number WorkflowID, Number Limit, Number Offset, String Status, String BatchID) throws IOException, ParseException {

        String FullURL = session.getURL()+"/cases?workflow_id="+WorkflowID.intValue()+"&limit="+Limit.intValue()+"&offset="+Offset.intValue()+"&status="+Status+"&source_batch_id="+BatchID;

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet request = new HttpGet(FullURL);

        request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

        HttpResponse response = httpClient.execute(request);
        String JsonResponse = EntityUtils.toString(response.getEntity());
        //System.out.println("DEBUG Response:"+JsonResponse);
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
        //System.out.println("DEBUG:"+JsonResponse);
        return jobj;
    }
    public JSONObject GettBatchInfo(VidadoSession session,String BatchID) throws IOException, ParseException {

        String FullURL = session.getURL()+"/batch/"+BatchID;
        //System.out.println("DEBUG Full URL:"+FullURL);
        HttpClient httpClient = HttpClientBuilder.create().build();

            HttpGet request = new HttpGet(FullURL);

            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            return jobj;

    }

    public JSONObject SubmitBatch(VidadoSession session,String BatchID) throws IOException, ParseException {

        String FullURL = session.getURL()+"/batch/"+BatchID+"/submit";

        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(FullURL);

            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            return jobj;

        } catch (Exception e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
        } finally {
        }

    }

    public JSONObject CreateBatch(VidadoSession session,String BatchName) throws IOException, ParseException {

        String FullURL = session.getURL()+"/batch/";

        //System.out.println("DEBUG:"+FullURL);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(FullURL);
            //String JSONString = "{\"name\":\""+BatchName+"\",\"sorting_enabled\":true,\"is_sorting_only\":false}";
            String payload = "{" +
                    "\"name\": \""+BatchName+"\", " +
                    "\"sorting_enabled\": true, " +
                    "\"is_sorting_only\":false" +
                    "}";
            StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);

            //StringEntity params = new StringEntity(JSONString);
            //System.out.println("DEBUG Payload:"+payload);
            request.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            String JsonResponse = EntityUtils.toString(response.getEntity());
            //System.out.println("DEBUG Response:"+JsonResponse);
            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            return jobj;

        } catch (Exception e) {
            throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
        } finally {
            // @Deprecated httpClient.getConnectionManager().shutdown();
        }

    }

    public JSONObject UploadDocumentToBatch(VidadoSession session, String BatchID, String FilePath){

            String CompleteURL =  session.getURL()+"/batch/"+BatchID+"/batch-file/";

            try
            {
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpPost uploadFile = new HttpPost(CompleteURL);
                uploadFile.addHeader("Captricity-API-Token", session.getSecureToken().getInsecureString());

                MultipartEntityBuilder builder = MultipartEntityBuilder.create();

                File f = new File(FilePath);
                builder.addBinaryBody("uploaded_file", new FileInputStream(f), ContentType.APPLICATION_OCTET_STREAM, f.getName());

                HttpEntity multipart = builder.build();
                uploadFile.setEntity(multipart);
                CloseableHttpResponse response = httpClient.execute(uploadFile);
                String JsonResponse = EntityUtils.toString(response.getEntity());
                //System.out.println("DEBUG Response:"+JsonResponse);
                JSONParser parse = new JSONParser();
                JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
                return jobj;

            }
            catch (Exception e) {
                throw new BotCommandException(MESSAGES.getString("APIError",e)) ;
            }

    }

}
