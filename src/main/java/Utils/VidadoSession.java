package Utils;

import com.automationanywhere.core.security.SecureString;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;

public class VidadoSession {

    public SecureString getSecureToken() {
        return this.VidadoAPIToken;
    }
    public String getURL() {
        return this.BaseURL;
    }

    private SecureString VidadoAPIToken;
    private String APIVersion;
    private String RootURL = "https://shreddr.captricity.com/api";
    private String BaseURL;

    public VidadoSession(SecureString VidadoAPIToken,String APIVersion){
        this.VidadoAPIToken = VidadoAPIToken;
        this.APIVersion = APIVersion;
        this.BaseURL = this.RootURL+"/"+APIVersion;

    }


}
