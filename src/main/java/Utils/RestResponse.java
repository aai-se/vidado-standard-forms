package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RestResponse{

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public static Table ProcessCaseDetailsResponseAsCsv(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        Table table = new Table();

        List<Row> AllRows = new ArrayList<Row>();

        // Setting up Headers

        List<Schema> ListOfHeaders = new ArrayList<Schema>();
        ListOfHeaders.add(new Schema("fieldName"));
        ListOfHeaders.add(new Schema("value"));
        ListOfHeaders.add(new Schema("confidence"));
        ListOfHeaders.add(new Schema("dataType"));
        ListOfHeaders.add(new Schema("fileName"));
        table.setSchema(ListOfHeaders);

        // Setting up rows
        String fileName = (String) jobj.get("name");
        JSONArray AllCases = (JSONArray) jobj.get("forms");
        JSONObject AllDataContainer = (JSONObject) AllCases.get(0);
        JSONArray AllFormData = (JSONArray) AllDataContainer.get("data");

        for(int i=0;i<AllFormData.size();i++){

            JSONObject aShred = (JSONObject) AllFormData.get(i);

            Double Confidence = (Double) aShred.get("confidence");
            String DataType = (String) aShred.get("data_type");
            String Value = (String) aShred.get("value");
            String FieldName = (String) aShred.get("field_name");
            if(Confidence == null){
                Confidence = 0D;
            }

            Row aRow = new Row();
            List<Value> ListOfValues = new ArrayList<Value>();
            ListOfValues.add(new StringValue(FieldName));
            ListOfValues.add(new StringValue(Value));
            ListOfValues.add(new StringValue(Confidence.toString()));
            ListOfValues.add(new StringValue(DataType));
            ListOfValues.add(new StringValue(fileName));

            aRow.setValues(ListOfValues);
            AllRows.add(aRow);
        }

        table.setRows(AllRows);
        return table;
    }

    public static ArrayList<HashMap<String, Value>> ProcessCaseDetailsResponse(String JsonResponse) throws ParseException {

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        ArrayList<HashMap<String,Value>> ListOfShreds = new ArrayList<HashMap<String,Value>>();

        String fileName = (String) jobj.get("name");
        JSONArray AllCases = (JSONArray) jobj.get("forms");
        JSONObject AllDataContainer = (JSONObject) AllCases.get(0);
        JSONArray AllFormData = (JSONArray) AllDataContainer.get("data");

        for(int i=0;i<AllFormData.size();i++){

            HashMap<String, Value> ResponseMap = new HashMap<String, Value>();

            JSONObject aShred = (JSONObject) AllFormData.get(i);
            System.out.println("DEBUG Shred:"+aShred);
            Double Confidence = (Double) aShred.get("confidence");
            //Confidence = Confidence*100;
            String DataType = (String) aShred.get("data_type");
            String Value = (String) aShred.get("value");
            String FieldName = (String) aShred.get("field_name");
            if(Confidence == null){
                Confidence = 0D;
            }

            ResponseMap.put("confidence",  new StringValue(Confidence.toString()));
            ResponseMap.put("dataType",  new StringValue(DataType));
            ResponseMap.put("value",  new StringValue(Value));
            ResponseMap.put("fieldName",  new StringValue(FieldName));
            ResponseMap.put("fileName", new StringValue(fileName));

            ListOfShreds.add(ResponseMap);
        }

        return ListOfShreds;
    }

    public static ArrayList<HashMap<String, Value>> ProcessCaseListResponse(JSONObject JsonResponse) {
        JSONArray AllCases = (JSONArray) JsonResponse.get("results");
        ArrayList<HashMap<String,Value>> ListOfCases = new ArrayList<HashMap<String,Value>>();

        for(int i=0;i<AllCases.size();i++){

            HashMap<String, Value> ResponseMap = new HashMap<String, Value>();

            JSONObject aCase = (JSONObject) AllCases.get(i);

            Long CaseID = (Long) aCase.get("id");
            String CaseFileName = (String) aCase.get("name");
            Long NumberOfFields = (Long) aCase.get("shred_count");
            Long SourceBatchID = (Long) aCase.get("source_batch_id");

            ResponseMap.put("caseId",  new StringValue(CaseID.toString()));
            ResponseMap.put("fileName",  new StringValue(CaseFileName));
            ResponseMap.put("numberOfFields",  new StringValue(NumberOfFields.toString()));
            ResponseMap.put("SourceBatchId",  new StringValue(SourceBatchID.toString()));

            ListOfCases.add(ResponseMap);
        }

        return ListOfCases;
    }

    public static Long ProcessCaseCountResponse(JSONObject JsonResponse) {
        Long CaseCount = (Long) JsonResponse.get("count");
        return CaseCount;
    }

    public static HashMap<String, Value> ProcessBatchInfoResponse(JSONObject JsonResponse) {

        String IsMessage = (String) JsonResponse.get("message");
        if(IsMessage != null){
            throw new BotCommandException(MESSAGES.getString("APIError",JsonResponse.toString()));
        }

        String Status = (String) JsonResponse.get("status");
        Long FileCount = (Long) JsonResponse.get("file_count");

        HashMap<String, Value> ResponseMap = new HashMap<String, Value>();
        ResponseMap.put("status",  new StringValue(Status));
        ResponseMap.put("fileCount",  new StringValue(FileCount.toString()));
        return ResponseMap;
    }

    public static HashMap<String, Value> ProcessCreateBatchResponse(JSONObject JsonResponse) {
        Long BatchID = (Long) JsonResponse.get("id");
        HashMap<String, Value> ResponseMap = new HashMap<String, Value>();
        String id = Long.toString(BatchID);
        ResponseMap.put("id",  new StringValue(id));
        return ResponseMap;
    }

    public static HashMap<String, Value> ProcessBatchSubmitResponse(JSONObject JsonResponse) {

        HashMap<String, Value> ResponseMap = new HashMap<String, Value>();
        String Status = (String) JsonResponse.get("status");

        if(Status != null){
            ResponseMap.put("status",  new StringValue(Status));
            ResponseMap.put("error",  new StringValue(""));
        }else{
            //System.out.println("DEBUG Raw0:"+JsonResponse.toString());
            JSONObject ReadinessJsonObj = (JSONObject) JsonResponse.get("readiness");
            //System.out.println("DEBUG Raw:"+ReadinessJsonObj.toString());
            String StatusInReadiness = (String) ReadinessJsonObj.get("status");
            JSONArray Errors = (JSONArray) ReadinessJsonObj.get("errors");

            String Error = "";
            if(Errors.size()>0){
                Error = (String) Errors.get(0);
                //System.out.println("DEBUG Err:"+Error);
            }

            ResponseMap.put("status",  new StringValue(StatusInReadiness));
            ResponseMap.put("error",  new StringValue(Error));
        }



        return ResponseMap;
    }

    public static HashMap<String, Value> ProcessFileUploadResponse(JSONObject JsonResponse) {
        String FileName = (String) JsonResponse.get("file_name");
        String UUID = (String) JsonResponse.get("uuid");
        Long PageCount = (Long) JsonResponse.get("page_count");
        String RejectReason = (String) JsonResponse.get("reject_reason");

        HashMap<String, Value> ResponseMap = new HashMap<String, Value>();

        String PageCountStr = Long.toString(PageCount);

        ResponseMap.put("fileName",  new StringValue(FileName));
        ResponseMap.put("uuid",  new StringValue(UUID));
        ResponseMap.put("pageCount",  new StringValue(PageCountStr));
        ResponseMap.put("rejectReason",  new StringValue(RejectReason));

        return ResponseMap;
    }

}