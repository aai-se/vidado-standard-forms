package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.DataType.*;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Get Case Data as Table",
        name="GetCaseDataAsTable",
        description="Get Case Data as Table",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        return_type= TABLE,
        group_label = "Converters",
        return_label="Table of results",
        return_required=true
)


public class CaseDataConverterToCsv {

    private static final Logger logger = LogManager.getLogger(CaseDataConverterToCsv.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public TableValue action(
            //@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "Json Case Details", default_value_type = STRING) @NotEmpty String JsonCaseDetails
    )
    {

        RestRequests restRequests = new RestRequests();

        Table tableRes = new Table();
        TableValue TableRetVal = new TableValue();
        try{

            tableRes = RestResponse.ProcessCaseDetailsResponseAsCsv(JsonCaseDetails);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }

        TableRetVal.set(tableRes);
        return TableRetVal;


    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
