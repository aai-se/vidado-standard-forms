package com.automationanywhere.botcommand.demo;

import Utils.VidadoSession;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.core.security.SecureString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */


@BotCommand
@CommandPkg(
        label = "Session Start",
        name = "SessionStart",
        description = "Session Start",
        icon = "vidado.svg",
        node_label = "Start | {{sessionName}}",
        group_label = "Admin",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1"
)
public class A_SessionStart {

    private static final Logger logger = LogManager.getLogger(A_SessionStart.class);
    private static final String CurrentAPIVersion = "v1";
    @Sessions
    private Map<String, Object> sessions = new HashMap<String,Object>();

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");


    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    private com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = AttributeType.CREDENTIAL) @Pkg(label = "Vidado API Token",  default_value_type = STRING, default_value = "") @NotEmpty SecureString APIToken
    ){

        // Check for existing session
        if (this.sessions.containsKey(sessionName)){
            throw new BotCommandException(MESSAGES.getString("SessionNameInUse",sessionName)) ;
        }
        logger.info("Vidado Package - Creating Session: "+sessionName);
        VidadoSession bms = new VidadoSession(APIToken,CurrentAPIVersion);
        this.sessions.put(sessionName, bms);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
