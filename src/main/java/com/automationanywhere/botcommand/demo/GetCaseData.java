package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Get Case Details",
        name="GetCaseDetails",
        description="Get Case Details",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        node_label="Get Case Details",
        group_label = "Cases",
        return_type= STRING,
        return_label="JSON Output",
        return_required=true
)

public class GetCaseData {

    private static final Logger logger = LogManager.getLogger(GetCaseData.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.NUMBER) @Pkg(label = "Case ID", default_value_type = NUMBER) @NotEmpty Number CaseID
    )
    {

        VidadoSession serv = (VidadoSession) this.sessions.get(sessionName);
        RestRequests restRequests = new RestRequests();

        StringValue RetString = new StringValue();


        try{
            JSONObject JsonResponse = restRequests.GetCaseInfo(serv,CaseID);
            RetString.set(JsonResponse.toJSONString());

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("JSONParsingError",e.getMessage())) ;
        }catch(IOException f){
            throw new BotCommandException(MESSAGES.getString("APIError",f.getMessage())) ;
        }

        return RetString;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
