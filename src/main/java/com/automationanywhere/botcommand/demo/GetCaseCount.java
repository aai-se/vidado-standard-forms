package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.*;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Get Case Count",
        name="GetCaseCount",
        description="Get Case Count",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        node_label="Get Case Count",
        group_label = "Cases",
        return_type= NUMBER,
        return_label="Number of open cases",
        return_required=true
)

public class GetCaseCount {

    private static final Logger logger = LogManager.getLogger(GetCaseCount.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public NumberValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.NUMBER) @Pkg(label = "Workflow ID", default_value_type = NUMBER) @NotEmpty Number WorkflowID,
            @Idx(index = "3", type = AttributeType.CHECKBOX) @Pkg(label = "Filter on Batch ID", default_value_type = BOOLEAN,default_value = "False")  Boolean FilterOnBatchID,
            @Idx(index = "3.1", type = AttributeType.TEXT) @Pkg(label = "Batch ID", default_value_type = STRING,default_value = "") @NotEmpty String BatchID
    )
    {

        VidadoSession serv = (VidadoSession) this.sessions.get(sessionName);
        RestRequests restRequests = new RestRequests();

        Long CaseCount = 0L;

        try{
            JSONObject JsonResponse;
            if(FilterOnBatchID){
                JsonResponse = restRequests.GetCaseListFromBatchID(serv,WorkflowID,1,0,"ready+to+export",BatchID);
            }else{
                JsonResponse = restRequests.GetCaseList(serv,WorkflowID,1,0,"ready+to+export");
            }


            CaseCount = RestResponse.ProcessCaseCountResponse(JsonResponse);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("JSONParsingError",e.getMessage())) ;
        }catch(IOException f){
            throw new BotCommandException(MESSAGES.getString("APIError",f.getMessage())) ;
        }

        NumberValue retNum = new NumberValue();
        retNum.set(CaseCount.doubleValue());
        return retNum;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
