package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.*;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Get Case Data as List",
        name="GetCaseDataAsList",
        description="Get Case Data as List",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        node_label="Get Case Data as List",
        group_label = "Converters",
        return_type= LIST,
        return_sub_type = ANY,
        return_label="List of Dictionaries with 5 keys: confidence, dataType, value, fieldName, fileName",
        return_required=true
)


public class CaseDataConverter {

    private static final Logger logger = LogManager.getLogger(CaseDataConverter.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<?> action(
            //@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "Json Case Details", default_value_type = STRING) @NotEmpty String JsonCaseDetails
    )
    {

        RestRequests restRequests = new RestRequests();
        HashMap<String, Value> ResultMap = new HashMap<String,Value>();
        ArrayList<HashMap<String,Value>> ListOfShreds = new ArrayList<HashMap<String,Value>>();

        try{

            ListOfShreds = RestResponse.ProcessCaseDetailsResponse(JsonCaseDetails);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("APIError",e.getMessage())) ;
        }

        ListValue AllObjects = new ListValue();
        ArrayList<DictionaryValue> myListOfDict = new ArrayList<DictionaryValue>();
        for(int i=0;i<ListOfShreds.size();i++){
            HashMap<String,Value> myMap = ListOfShreds.get(i);
            DictionaryValue dv = new DictionaryValue();
            dv.set(myMap);
            myListOfDict.add(dv) ;
        }

        AllObjects.set(myListOfDict);
        return AllObjects;


    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
