package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Submit Batch",
        name="SubmitBatch",
        description="Submit Batch",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        node_label="Submit Batch",
        group_label = "Batches",
        return_type= DataType.DICTIONARY,
        return_label="Output Dictionary with 3 keys: errors, readiness, status",
        return_required=true
)

public class SubmitBatch {

    private static final Logger logger = LogManager.getLogger(SubmitBatch.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Batch ID", default_value_type = STRING) @NotEmpty String BatchID
    )
    {
        if("".equals(BatchID)) {throw new BotCommandException(MESSAGES.getString("batchIDMissing"));}

        VidadoSession serv = (VidadoSession) this.sessions.get(sessionName);
        RestRequests restRequests = new RestRequests();
        HashMap<String, Value> ResultMap = new HashMap<String,Value>();
        DictionaryValue ReturnDictionary = new DictionaryValue();

        try{
            JSONObject JsonResponse = restRequests.SubmitBatch(serv,BatchID);

            ResultMap = RestResponse.ProcessBatchSubmitResponse(JsonResponse);
            ReturnDictionary.set(ResultMap);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("JSONParsingError",e.getMessage())) ;
        }catch(IOException f){
            throw new BotCommandException(MESSAGES.getString("APIError",f.getMessage())) ;
        }

        return ReturnDictionary;

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
