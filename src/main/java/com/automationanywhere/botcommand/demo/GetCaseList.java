package com.automationanywhere.botcommand.demo;

import Utils.RestRequests;
import Utils.RestResponse;
import Utils.VidadoSession;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.*;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(
        label="Get Case List",
        name="GetCaseList",
        description="Get Case List",
        icon="vidado.svg",
        comment = true,
        text_color="#108ad1",
        background_color = "#108ad1",
        node_label="Get Case List",
        group_label = "Cases",
        return_type= DataType.LIST,
        return_sub_type = DataType.ANY,
        return_label="List of Dictionaries with 4 keys: caseId, fileName, numberOfFields, caseBatchId",
        return_required=true
)

public class GetCaseList {

    private static final Logger logger = LogManager.getLogger(GetCaseList.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<?> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.NUMBER) @Pkg(label = "Workflow ID", default_value_type = NUMBER) @NotEmpty Number WorkflowID,
            @Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Limit", default_value_type = NUMBER,default_value = "10") @NotEmpty Number Limit,
            @Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Offset", default_value_type = NUMBER,default_value = "0") @NotEmpty Number Offset,
            @Idx(index = "5", type = AttributeType.CHECKBOX) @Pkg(label = "Filter on Batch ID", default_value_type = BOOLEAN,default_value = "false")  Boolean FilterOnBatchID,
            @Idx(index = "5.1", type = AttributeType.TEXT) @Pkg(label = "Batch ID", default_value_type = STRING,default_value = "") @NotEmpty String BatchID
    )
    {

        VidadoSession serv = (VidadoSession) this.sessions.get(sessionName);
        RestRequests restRequests = new RestRequests();
        HashMap<String, Value> ResultMap = new HashMap<String,Value>();
        ArrayList<HashMap<String,Value>> ListOfCases = new ArrayList<HashMap<String,Value>>();

        JSONObject JsonResponse;
        try{
            if(FilterOnBatchID){
                JsonResponse = restRequests.GetCaseListFromBatchID(serv,WorkflowID,Limit,Offset,"ready+to+export",BatchID);
            }else{
                JsonResponse = restRequests.GetCaseList(serv,WorkflowID,Limit,Offset,"ready+to+export");
            }

            ListOfCases = RestResponse.ProcessCaseListResponse(JsonResponse);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("JSONParsingError",e.getMessage())) ;
        }catch(IOException f){
            throw new BotCommandException(MESSAGES.getString("APIError",f.getMessage())) ;
        }

        ListValue AllObjects = new ListValue();
        ArrayList<DictionaryValue> myListOfDict = new ArrayList<DictionaryValue>();
        for(int i=0;i<ListOfCases.size();i++){
            HashMap<String,Value> myMap = ListOfCases.get(i);
            DictionaryValue dv = new DictionaryValue();
            dv.set(myMap);
            myListOfDict.add(dv) ;
        }

        AllObjects.set(myListOfDict);
        return AllObjects;
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
